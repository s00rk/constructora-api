from django.conf.urls import url, include
from django.contrib import admin
from jwt_auth.views import obtain_jwt_token
from django.conf import settings
from django.conf.urls.static import static

import inspect
from tastypie.resources import ModelResource
from tastypie.api import Api
import api.catalogos.resource
import api.constructora.resource
from api.outlook.resource import *


v1_api = Api(api_name='v1')
its = []
for x in dir(api.catalogos.resource):
	item = getattr(api.catalogos.resource, x)
	if inspect.isclass(item) and issubclass(item, ModelResource) and x != 'ModelResource' and not str(item) in its:
		v1_api.register(item())
		its.append( str(item) )
for x in dir(api.constructora.resource):
	item = getattr(api.constructora.resource, x)
	if inspect.isclass(item) and issubclass(item, ModelResource) and x != 'ModelResource' and not str(item) in its:
		v1_api.register(item())
		its.append( str(item) )

v1_api.register(EmailResource())

urlpatterns = [
	url(r'^api/', include(v1_api.urls)),
	url(r'^auth/', obtain_jwt_token),
    url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
