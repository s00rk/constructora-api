# -*- encoding: utf-8 -*-
from tastypie.resources import Resource
from tastypie import fields
from django.conf.urls import url
from catalogos.models import UserEmail

from tastypie.authentication import BasicAuthentication, MultiAuthentication
from tastypie.authorization import DjangoAuthorization
from api.utils.JWTAuthentication import JWTAuthentication
from tastypie.exceptions import TastypieError
from tastypie.http import HttpBadRequest

from datetime import datetime
from outlook.outlookservice import Outlook
from email.header import decode_header, make_header
import json, email, math, time
from bs4 import BeautifulSoup as bs
from django.core.files.uploadedfile import InMemoryUploadedFile
from constructora.models import Presupuesto

def json_serial(obj):
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    return obj

class faillogin(TastypieError):
    def __init__(self, data="", message=""):
        self._response = {'exito':False,'message':message or 'no esta activo'}
    @property
    def response(self):
        return HttpBadRequest(
            json.dumps(self._response),
            content_type='application/json')

class EmailObject(object):
    def __init__(self, d):
        self.__dict__['d'] = d

    def __getattr__(self, key):
        value = self.__dict__['d'][key]
        if type(value) == type({}):
            return EmailObject(value)

        return value

class MultiPartResource(object):
   def deserialize(self, request, data, format=None):
       if not format:
           format = request.Meta.get('CONTENT_TYPE', 'application/json')
       if format == 'application/x-www-form-urlencoded':
           return request.POST
       if format.startswith('multipart'):
           data = request.POST.copy()
           data.update(request.FILES)
           return data
       return super(MultiPartResource, self).deserialize(request, data, format)

class EmailResource(MultiPartResource, Resource):
    id = fields.IntegerField(attribute="id", null=True)
    tema = fields.CharField(attribute="tema", null=True)
    desde = fields.CharField(attribute="desde", null=True)
    para = fields.CharField(attribute="para", null=True)
    fecha = fields.DateTimeField(attribute="fecha", null=True)
    cuerpo = fields.CharField(attribute="cuerpo", null=True, use_in='detail')
    folder = fields.CharField(attribute='folder', null=True)
    visto = fields.BooleanField(attribute='visto', null=True)

    class Meta:
        resource_name = 'emails'
        include_resource_uri = False
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'post']

    def alter_list_data_to_serialize(self, request, data):
        data['meta']['total_count'] = self.total_count
        return super(EmailResource, self).alter_list_data_to_serialize(request, data)


    def getMailbox(self, request):
        mailbox = request.GET.get("mailbox")
        if mailbox is None or len(mailbox) == 0:
            mailbox = "inbox"

        mailbox = mailbox.capitalize()
        us, created = UserEmail.objects.get_or_create(user=request.user, defaults={'inbox': '[]', 'sent':'[]'})
        if mailbox == 'Inbox':
            self.js = json.loads( us.inbox )
        elif mailbox == 'Sent':
            self.js = json.loads( us.sent )

        self.ret = self.convertToEmail( self.js )

        return (mailbox.capitalize())

    def obj_get(self, request=None, **kwargs):
        folder =  self.getMailbox( kwargs['bundle'].request )
        self.mail = Outlook()
        resp = self.mail.login(kwargs['bundle'].request.user.perfil.outlook_user, kwargs['bundle'].request.user.perfil.outlook_pass)
        if not resp:
            raise faillogin(message="Usuario/Contraseña incorrecta")
        cant = self.mail.select(folder)

        for o in self.ret:
            if o.id == int(kwargs['pk']):
                x = o.id
                fecha, para, desde, tema, cuerpo = self.getEmail(x)

                obj = EmailObject({
                    'pk': x,
                    'id': x,
                    'tema':  tema.decode('utf8'),
                    'desde': desde,
                    'para': para,
                    'fecha': fecha,
                    'cuerpo': cuerpo,
                    'folder': folder,
                    'visto': True
                })
                self.setVisto(x, kwargs['bundle'].request, folder)
                return obj
        return None

    def setVisto(self, id, request, folder):
        us = UserEmail.objects.get(user=request.user)
        for j in self.js:
            if j['id'] == id:
                j['visto'] = True
        if folder == 'Inbox':
            us.inbox = json.dumps( self.js, default=json_serial )
        elif folder == 'Sent':
            us.sent = json.dumps( self.js, default=json_serial )
        us.save()

    def obj_get_list(self, bundle, **kwargs):
        self.ret = []
        self.js = []

        if bundle.request is None or bundle.request.user is None:
            return self.ret

        folder = self.getMailbox( bundle.request )
            
        self.mail = Outlook()
        resp = self.mail.login(bundle.request.user.perfil.outlook_user, bundle.request.user.perfil.outlook_pass)
        if not resp:
            raise faillogin(message="Usuario/Contraseña incorrecta")

        cant = self.mail.select(folder)
        page = int(bundle.request.GET.get('page', 1))
        unseen = self.mail.imap.search(None, "UNSEEN")[1][0].split()

        up =  cant - (10*(page-1))
        down = up - 9
        if down <= 0:
            down = 1

        self.total_count = cant
        self.ret, self.js = self.inbox(folder, self.ret, self.js, down, up, unseen)
                
        us = UserEmail.objects.get(user=bundle.request.user)
        if folder == 'Inbox':
            us.inbox = json.dumps( self.js, default=json_serial )
        elif folder == 'Sent':
            us.sent = json.dumps( self.js, default=json_serial )
        us.save()
        return self.returnByPage(up, down, bundle.request, folder)

    def returnByPage(self, up, down, request, folder):
        l = []
        a = []
        for c in self.ret:
            if c.id >= down and c.id <= up:
                l.append(c)
                a.append(c.__dict__['d'])
        l = sorted(l, key=lambda k: k.id, reverse=True)
        a = sorted(a, key=lambda k: k.get('id', 0), reverse=True)

        if len(self.ret) >= 50:
            us = UserEmail.objects.get(user=request.user)
            if folder == 'Inbox':
                us.inbox = json.dumps( a, default=json_serial )
            elif folder == 'Sent':
                us.sent = json.dumps( a, default=json_serial )
            us.save()
        return l

    def convertToEmail(self, js):
        ret = []
        for x in js:
            x['fecha'] = datetime.strptime(x['fecha'], '%Y-%m-%dT%H:%M:%S')
            ret.append( EmailObject(x) )
        return ret

    def decode_headers(self, value):
        return ' '.join((item[0].decode(item[1] or 'utf-8').encode('utf-8') for item in decode_header(value)))

    def getEmail(self, id):
        em = self.mail.getEmail(id)
        fecha = em.get('Date')
        if em is not None:
            date_tuple = email.utils.parsedate_tz(fecha)
            if date_tuple:
                fecha = datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))

        para = em.get('To')
        if para is not None:
            para = self.decode_headers(para).replace('\r\n\t', ' ')

        desde = em.get('From')
        if desde is not None:
            desde = self.decode_headers(desde).replace('\r\n\t', ' ')

        tema = em.get('Subject')
        if tema is not None:
            tema = self.decode_headers(tema)

        text = ''
        html = ''
        cuerpo = ''
        if em.is_multipart():
            for e in em.get_payload():
                charset = e.get_content_charset()
                if charset is None:
                    charset = 'utf-8'
                if e.get_content_type() == 'text/html':
                    html = e.get_payload(decode=True).decode(charset)
                    s = bs(html, "html.parser")
                    html = s.prettify().replace('\n', '').replace('\t', '').replace('\r', '')

                elif e.get_content_type() == 'text/plain':
                    text = e.get_payload(decode=True).decode(charset).replace('\n', '').replace('\t', '').replace('\r', '')

        else:
            cuerpo = em.get_payload()

        if cuerpo == '':
            if html != '':
                cuerpo = html
            else:
                cuerpo = text
        return (fecha, para, desde, tema, cuerpo)

    def existeItem(self, x, ret):
        for o in ret:
            if o.id == x:
                return True
        return False

    def inbox(self, folder, ret, js, down, up, unseen):
        for x in range(down, up+1):
            if self.existeItem(x, ret):
                continue
            fecha, para, desde, tema, cuerpo = self.getEmail(x)
            v = True
            if str(x) in unseen:
                self.mail.imap.store(x, '-FLAGS','\\Seen')
                v = False

            obj = EmailObject({
                'pk': x,
                'id': x,
                'tema':  tema,
                'desde': desde,
                'para': para,
                'fecha': fecha,
                'cuerpo': '',
                'folder': folder,
                'visto': v
            })
            ret.append( 
                obj
            )
            js.append(obj.__dict__['d'])
        js = sorted(js, key=lambda k: k.get('id', 0), reverse=True)
        return (ret, js)

    def obj_create(self, bundle, request = None, **kwargs):
        self.mail = Outlook()
        resp = self.mail.login(bundle.request.user.perfil.outlook_user, bundle.request.user.perfil.outlook_pass)
        if not resp:
            raise faillogin(message="Usuario/Contraseña incorrecta")
        if not "presupuesto_id" in bundle.data:
            if bundle.data['es_respuesta'] == True or bundle.data['es_respuesta'] == 'true':
                self.mail.send(bundle.data['para'], bundle.data['tema'], bundle.data['mensaje'])
            else:
                files = []
                for x in bundle.data:
                    if isinstance(bundle.data[x], InMemoryUploadedFile):
                        files.append( bundle.data[x] )
                self.mail.sendAttach(bundle.data['para'], bundle.data['tema'], bundle.data['mensaje'], files)
        else:
            presupuesto = Presupuesto.objects.get(pk=int(bundle.data['presupuesto_id']))
            files = [presupuesto.pdf]
            para = presupuesto.para.email
            for p in presupuesto.contactos.all():
                para += ',' + p.email
            self.mail.sendAttach(para, 'Presupuesto ' + presupuesto.nombre, bundle.data['mensaje'], files)