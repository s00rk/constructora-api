from tastypie.authentication import Authentication
from jwt_auth.utils import jwt_decode_handler
from jwt_auth.mixins import JSONWebTokenAuthMixin

class JWTAuthentication(Authentication):
    def is_authenticated(self, request, **kwargs):
        try:
        	authMetod = JSONWebTokenAuthMixin()
        	authMetod.authenticate(request)
        	return True
        except:
        	return False

    def get_identifier(self, request):
    	authMetod = JSONWebTokenAuthMixin()
        request.user, request.token = authMetod.authenticate(request)
        return request.user