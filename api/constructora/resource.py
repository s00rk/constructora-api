from django.contrib.auth.models import User, Group
from django.db.models import Q

from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import BasicAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from api.utils.JWTAuthentication import JWTAuthentication
from tastypie import fields
from tastypie.validation import FormValidation

from catalogos.models import *
from constructora.models import *
from api.catalogos.resource import *
from .validations import *

class PresupuestoResource(ModelResource):
    cliente = fields.ForeignKey(ClienteResource, 'cliente', null=True, full=True)
    sucursal = fields.ForeignKey(SucursalResource, 'sucursal', null=True, full=True)
    contactos = fields.ToManyField(ContactoResource, 'contactos', full=True, null=True, use_in='detail')
    para = fields.ForeignKey(ContactoResource, 'para', full=True, null=True, use_in='detail')

    obras = fields.ToManyField('api.constructora.resource.ObraResource', 'obra_set', related_name='presupuesto', null=True, full=True)
    class Meta:
        always_return_data = True
        queryset = Presupuesto.objects.all()
        resource_name = 'presupuestos'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Presupuesto._meta.get_fields()
        filtering = {
            'nombre': ALL,
            'cliente': ALL_WITH_RELATIONS,
            'fecha': ALL
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
    def apply_filters(self, request, applicable_filters):
        base_object_list = super(PresupuestoResource, self).apply_filters(request, applicable_filters)
        query = request.GET.get('query', None)
        fecha_inicio = request.GET.get('fecha_inicio', None)
        fecha_final = request.GET.get('fecha_final', None)
        if query:
            qset = (
                Q(nombre__icontains=query) |
                Q(cliente__razon_social__icontains=query) |
                Q(para__nombre__icontains=query)
            )
            base_object_list = base_object_list.filter(qset).distinct()
        if fecha_inicio and fecha_final:
            base_object_list = base_object_list.filter(fecha__range=[fecha_inicio, fecha_final])
        return base_object_list

class ObraResource(ModelResource):
    presupuesto = fields.ForeignKey(PresupuestoResource, 'presupuesto', full=False)

    items = fields.ToManyField('api.constructora.resource.ObraItemResource', 'obraitem_set', related_name='obra', null=True, full=True)
    class Meta:
        always_return_data = True
        queryset = Obra.objects.all()
        resource_name = 'obras'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Obra._meta.get_fields()
        filtering = {
            'nombre': ALL,
            'fecha': ALL,
            'presupuesto': ALL_WITH_RELATIONS
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()

class ObraItemResource(ModelResource):
    insumo = fields.ForeignKey(InsumoResource, 'insumo', full=True)
    unidad_medida = fields.ForeignKey(UnidadMedidaResource, 'unidad_medida', full=True)
    obra = fields.ForeignKey(ObraResource, 'obra', full=False)
    class Meta:
        always_return_data = True
        queryset = ObraItem.objects.all()
        resource_name = 'obras_item'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = ObraItem._meta.get_fields()
        filtering = {
            'concepto': ALL,
            'obra': ALL_WITH_RELATIONS
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()