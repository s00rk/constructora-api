from django.contrib.auth.models import User, Group
from django.db.models import Q

from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import BasicAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from api.utils.JWTAuthentication import JWTAuthentication
from tastypie import fields
from tastypie.validation import FormValidation

from catalogos.models import *
from .validations import *

class GroupResource(ModelResource):
    class Meta:
        queryset = Group.objects.all()
        allowed_methods = ['get']
        always_return_data = True
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        resource_name = 'grupos'
        filtering = {
            'name': ['all']
        }

class UserResource(ModelResource):
    grupos = fields.ToManyField(GroupResource, 'groups', full=True, readonly=True, null=True)
    class Meta:
        queryset = User.objects.all()
        allowed_methods = ['get']
        always_return_data = True
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        resource_name = 'usuarios'
        excludes = ['password', 'is_active']
        filtering = {
            'username': ALL,
            'email': ALL
        }

class EstadoResource(ModelResource):
    class Meta:
        always_return_data = True
        queryset = Estado.objects.all()
        resource_name = 'estados'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Estado._meta.get_fields()
        filtering = {
            'nombre': ALL,
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        validation = FormValidation(form_class=EstadoForm)

class MunicipioResource(ModelResource):
    estado = fields.ForeignKey(EstadoResource, 'estado', null=True, full=True)
    class Meta:
        always_return_data = True
        queryset = Municipio.objects.all()
        resource_name = 'municipios'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Municipio._meta.get_fields()
        filtering = {
            'nombre': ALL,
            'estado': ALL_WITH_RELATIONS
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        validation = FormValidation(form_class=MunicipioForm)
    def apply_filters(self, request, applicable_filters):
        base_object_list = super(MunicipioResource, self).apply_filters(request, applicable_filters)
        query = request.GET.get('query', None)
        if query:
            qset = (
                Q(nombre__icontains=query) |
                Q(estado__nombre__icontains=query)
            )
            base_object_list = base_object_list.filter(qset).distinct()
        return base_object_list

class DomicilioResource(ModelResource):
    municipio = fields.ForeignKey(MunicipioResource, 'municipio', full=True)
    class Meta:
        always_return_data = True
        queryset = Domicilio.objects.all()
        resource_name = 'domicilios'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Domicilio._meta.get_fields()
        filtering = {
            'calle': ALL,
            'colonia': ALL,
            'municipio': ALL_WITH_RELATIONS
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()

class UnidadMedidaResource(ModelResource):
    class Meta:
        always_return_data = True
        queryset = UnidadMedida.objects.all()
        resource_name = 'unidad_medidas'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = UnidadMedida._meta.get_fields()
        filtering = {
            'codigo': ALL,
            'nombre': ALL
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        validation = FormValidation(form_class=UnidadMedidaForm)

class PerfilResource(ModelResource):
    usuario = fields.ForeignKey(UserResource, 'usuario', full=True)
    class Meta:
        always_return_data = True
        queryset = Perfil.objects.all()
        resource_name = 'perfiles'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Perfil._meta.get_fields()
        excludes = ['outlook_pass']
        filtering = {
            'usuario': ALL_WITH_RELATIONS
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()

    def get_object_list(self, request):
        me = request.GET.get('me')
        ret = super(PerfilResource, self).get_object_list(request)
        if me == 'true':
            ret = ret.filter(usuario=request.user)
        return ret


class ContactoResource(ModelResource):
    class Meta:
        always_return_data = True
        queryset = Contacto.objects.all()
        resource_name = 'contactos'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Contacto._meta.get_fields()
        filtering = {
            'nombre': ALL,
            'telefono': ALL,
            'movil': ALL,
            'email': ALL
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        validation = FormValidation(form_class=ContactoForm)

class SucursalResource(ModelResource):
    class Meta:
        always_return_data = True
        queryset = Sucursal.objects.all()
        resource_name = 'sucursales'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Sucursal._meta.get_fields()
        filtering = {
            'nombre': ALL
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        validation = FormValidation(form_class=SucursalForm)

class ProveedorResource(ModelResource):
    total_contactos = fields.IntegerField(attribute='total_contactos', readonly=True)
    domicilio = fields.ForeignKey(DomicilioResource, 'domicilio', null=True, full=True, use_in='detail')
    contactos = fields.ToManyField(ContactoResource, 'contactos', full=True, null=True, use_in='detail')
    sucursales = fields.ToManyField(SucursalResource, 'sucursales', full=True, null=True)
    class Meta:
        always_return_data = True
        queryset = Proveedor.objects.all()
        resource_name = 'proveedores'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Proveedor._meta.get_fields()
        filtering = {
            'razon_social': ALL,
            'domicilio': ALL_WITH_RELATIONS,
            'contactos': ALL_WITH_RELATIONS
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        validation = FormValidation(form_class=ProveedorForm)

class ClienteResource(ModelResource):
    total_contactos = fields.IntegerField(attribute='total_contactos', readonly=True)
    domicilio = fields.ForeignKey(DomicilioResource, 'domicilio', null=True, full=True, use_in='detail')
    contactos = fields.ToManyField(ContactoResource, 'contactos', full=True, null=True, use_in='detail')
    sucursales = fields.ToManyField(SucursalResource, 'sucursales', full=True, null=True)
    class Meta:
        always_return_data = True
        queryset = Cliente.objects.all()
        resource_name = 'clientes'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Cliente._meta.get_fields()
        filtering = {
            'razon_social': ALL,
            'domicilio': ALL_WITH_RELATIONS,
            'contactos': ALL_WITH_RELATIONS
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        validation = FormValidation(form_class=ClienteForm)

class InsumoResource(ModelResource):
    unidad_medida = fields.ForeignKey(UnidadMedidaResource, 'unidad_medida', null=True, full=True)
    proveedor = fields.ForeignKey(ProveedorResource, 'proveedor', full=True, null=True)
    class Meta:
        always_return_data = True
        queryset = Insumo.objects.all()
        resource_name = 'insumos'
        allowed_methods = ['get', 'post', 'put', 'delete']
        ordering = Insumo._meta.get_fields()
        filtering = {
            'nombre': ALL,
            'precio': ALL,
            'unidad_medida': ALL_WITH_RELATIONS
        }
        authentication = MultiAuthentication(JWTAuthentication(), BasicAuthentication())
        authorization = Authorization()
        validation = FormValidation(form_class=InsumoForm)
    def apply_filters(self, request, applicable_filters):
        base_object_list = super(InsumoResource, self).apply_filters(request, applicable_filters)
        query = request.GET.get('query', None)
        if query:
            qset = (
                Q(nombre__icontains=query) |
                Q(proveedor__razon_social__icontains=query)
            )
            base_object_list = base_object_list.filter(qset).distinct()
        excludes = {}
        for param in request.GET:
            if param[0] == '!':
                excludes[param[1:]] = request.GET[param]
        return base_object_list.exclude(**excludes)