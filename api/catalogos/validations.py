# -*- encoding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from catalogos.models import *

class EstadoForm(forms.Form):
	nombre = forms.CharField(max_length=50, required=True, error_messages={'max_length': 'El campo es demasiado largo. max: %(limit_value)d'})

	def clean(self):
		cleaned_data = self.cleaned_data
		nombre = cleaned_data.get('nombre')
		estado = Estado.objects.filter(nombre=nombre)
		if "id" in self.data:
			estado = estado.exclude(id=self.data["id"])
		if estado.exists():
			self._errors['nombre'] = self.error_class(['Ya existe un estado con ese nombre'])
		return cleaned_data

class MunicipioForm(forms.Form):
	estado = forms.CharField(required=True, error_messages={'max_length': 'El campo es demasiado largo. max: %(limit_value)d'})
	nombre = forms.CharField(max_length=250, required=True, error_messages={'max_length': 'El campo es demasiado largo. max: %(limit_value)d'})

	def clean(self):
		cleaned_data = self.cleaned_data
		nombre = cleaned_data.get('nombre')
		if "estado" in self.data:
			municipio = Municipio.objects.filter(nombre=nombre, estado__id=self.data['estado']['id'])
			if "id" in self.data:
				municipio = municipio.exclude(id=self.data["id"])
			if municipio.exists():
				self._errors['nombre'] = self.error_class(['Ya existe un municipio en el estado '+ self.data['estado']['nombre'] +' con ese nombre'])
		return cleaned_data

class UnidadMedidaForm(forms.Form):
	codigo = forms.CharField(max_length=10, required=True, error_messages={'max_length': 'El campo es demasiado largo. max: %(limit_value)d'})
	nombre = forms.CharField(max_length=100, required=True, error_messages={'max_length': 'El campo es demasiado largo. max: %(limit_value)d'})

	def clean(self):
		cleaned_data = self.cleaned_data
		nombre = cleaned_data.get('nombre')
		municipio = UnidadMedida.objects.filter(nombre=nombre)
		if "id" in self.data:
			municipio = municipio.exclude(id=self.data["id"])
		if municipio.exists():
			self._errors['nombre'] = self.error_class(['Ya existe una unidad de medida con ese nombre'])
		return cleaned_data

class ContactoForm(forms.Form):
	nombre = forms.CharField(max_length=200, required=True, error_messages={'max_length': 'El campo es demasiado largo. max: %(limit_value)d'})
	telefono = forms.CharField(max_length=50, required=False)
	movil = forms.CharField(max_length=50, required=False)
	email = forms.CharField(max_length=254, required=False)

class SucursalForm(forms.Form):
	nombre = forms.CharField(max_length=500, required=True, error_messages={'max_length': 'El campo es demasiado largo. max: %(limit_value)d'})

class ProveedorForm(forms.Form):
	razon_social = forms.CharField(max_length=200, required=True)

	def clean(self):
		cleaned_data = self.cleaned_data
		razon_social = cleaned_data.get('razon_social')
		proveedor = Proveedor.objects.filter(razon_social=razon_social)
		if "id" in self.data:
			proveedor = proveedor.exclude(id=self.data["id"])
		if proveedor.exists():
			self._errors['razon_social'] = self.error_class(['Ya existe un proveedor con esa razón social'])
		return cleaned_data

class ClienteForm(forms.Form):
	razon_social = forms.CharField(max_length=200, required=True)

	def clean(self):
		cleaned_data = self.cleaned_data
		razon_social = cleaned_data.get('razon_social')
		cliente = Cliente.objects.filter(razon_social=razon_social)
		if "id" in self.data:
			cliente = cliente.exclude(id=self.data["id"])
		if cliente.exists():
			self._errors['razon_social'] = self.error_class(['Ya existe un cliente con esa razón social'])
		return cleaned_data

class InsumoForm(forms.Form):
	nombre = forms.CharField(max_length=100, required=True, error_messages={'max_length': 'El campo es demasiado largo. max: %(limit_value)d'})
	precio = forms.DecimalField(max_digits=8, decimal_places=2, required=True)

class InsumoProveedorForm(forms.Form):
	precio = forms.DecimalField(max_digits=8, decimal_places=2, required=True)