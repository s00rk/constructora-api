from django.db import models
from django.utils import timezone

class ConstructoraQuerySet(models.query.QuerySet):
	def delete(self):
		self.update(is_active=timezone.now())
	def active(self):
		return self.filter(is_active=None).order_by('-id')

class ConstructoraManager(models.Manager):
	def get_query_set(self):
		return ConstructoraQuerySet(self.model, using=self._db)
	def get_queryset(self):
		return self.get_query_set().active()

def BorradoLogico(self):
	self.is_active = timezone.now()
	super(type(self), self).save()	