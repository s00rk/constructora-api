# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-10 11:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalogos', '0019_auto_20160610_1106'),
        ('constructora', '0011_presupuesto_total_porcentaje'),
    ]

    operations = [
        migrations.AddField(
            model_name='presupuesto',
            name='sucursal',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='catalogos.Sucursal'),
        ),
    ]
