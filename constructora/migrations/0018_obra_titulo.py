# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-09-09 22:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('constructora', '0017_auto_20160909_2153'),
    ]

    operations = [
        migrations.AddField(
            model_name='obra',
            name='titulo',
            field=models.CharField(blank=True, max_length=300),
        ),
    ]
