# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from catalogos.models import *
from datetime import date
from uuid import uuid4
import os

from wkhtmltopdf.utils import render_pdf_from_template
from django.core.files import File
from django.template.loader import get_template
from django.dispatch import receiver
from django.db.models.signals import post_save

from api.constructora_api import ConstructoraManager, BorradoLogico

def guardarPdf(instance, filename):
	filename, ext = os.path.splitext(filename)
	carpeta = ''
	nombre = ''
	if instance.__class__.__name__ == 'Presupuesto':
		carpeta = 'presupuestos'
		if instance.pk:
			nombre = 'P' + str(instance.pk) + '__'
		else:
			nombre = 'P__'

	uuidd = uuid4().hex
	nombre += str(date.today().strftime('%d_%m_%y'))
	return 'reportes/{0}/{1}/{2}__{3}{4}'.format(carpeta, instance.cliente.razon_social, nombre, uuidd, ext)


class Presupuesto(models.Model):
	cliente = models.ForeignKey(Cliente, blank=True, null=True)
	sucursal = models.ForeignKey(Sucursal, blank=True, null=True)
	nombre = models.TextField()
	fecha = models.DateField()
	subtotal = models.DecimalField(max_digits=8, decimal_places=2)
	iva = models.DecimalField(max_digits=8, decimal_places=2)
	total = models.DecimalField(max_digits=8, decimal_places=2)
	estatus = models.IntegerField(default=0)
	pdf = models.FileField(upload_to=guardarPdf, null=True, blank=True)
	para = models.ForeignKey(Contacto, related_name='para', null=True, blank=True)
	contactos = models.ManyToManyField(Contacto, related_name='contactos', blank=True)

	nota = models.TextField(blank=True)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()

	def obras(self):
		return Obra.objects.filter(presupuesto=self)

	def delete(self):
		reporte = Presupuesto.objects.get(pk=self.pk)
		reporte.pdf.delete(False)
		BorradoLogico(self)

	def save(self, *args, **kwargs):
		template_name = "pdfs/presupuesto/presupuesto.html"
		header_template = "pdfs/presupuesto/header.html"
		footer_template = "pdfs/presupuesto/footer.html"
		if self.pk:
			if self.origin is None:
				self.origin = self.pk
			reporte = Presupuesto.objects.get(pk=self.pk)
			if reporte.pdf:
				reporte.pdf.delete(False)
		super(Presupuesto, self).save(*args, **kwargs)
		if self.origin is None:
			self.origin = self.pk

		filename = str(self.pk) + ".pdf"
		context = { 'reporte': self }

		responses = render_pdf_from_template(
			get_template(template_name),
			get_template(header_template),
			get_template(footer_template),
			context=context
		)

		with open(filename, 'wb') as f:
			f.write(responses)
		f = File(open(filename, 'rb'))

		self.pdf.save(filename, f, save=False)
		super(Presupuesto, self).save(*args, **kwargs)
		f.close()
		os.remove( os.path.join(os.getcwd(), filename) )

	def __unicode__(self):
		return u'%s %s' % (self.nombre, self.total)

class Obra(models.Model):
	nombre = models.TextField()
	total = models.DecimalField(max_digits=8, decimal_places=2)
	porcentaje = models.IntegerField(default=20)
	porcentaje_mano_obra = models.IntegerField(default=60)
	titulo = models.CharField(max_length=300, blank=True)

	presupuesto = models.ForeignKey(Presupuesto)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)	

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return u'%s %s' % (self.presupuesto, self.nombre)

class ObraItem(models.Model):
	insumo = models.ForeignKey(Insumo)
	unidad_medida = models.ForeignKey(UnidadMedida)
	cantidad = models.DecimalField(max_digits=8, decimal_places=2)
	precio_unitario = models.DecimalField(max_digits=8, decimal_places=2)
	importe = models.DecimalField(max_digits=8, decimal_places=2)

	obra = models.ForeignKey(Obra)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return u'%s %s %s' % (self.obra, self.insumo, self.importe)	