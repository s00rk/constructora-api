# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-10 10:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogos', '0017_remove_contacto_enviar'),
    ]

    operations = [
        migrations.AddField(
            model_name='insumo',
            name='costo',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True),
        ),
    ]
