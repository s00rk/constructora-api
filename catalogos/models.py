from __future__ import unicode_literals

from django.dispatch import receiver
from django.db.models.signals import post_save

from django.db import models
from django.contrib.auth.models import User

from api.constructora_api import ConstructoraManager, BorradoLogico


class UserEmail(models.Model):
	user = models.OneToOneField(User)
	inbox = models.TextField(blank=True, null=True)
	sent = models.TextField(blank=True, null=True)
	delete = models.TextField(blank=True, null=True)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)


class Estado(models.Model):
	nombre = models.CharField(max_length=50)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return self.nombre

class Municipio(models.Model):
	estado = models.ForeignKey(Estado)
	nombre = models.CharField(max_length=250)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return self.nombre

class Domicilio(models.Model):
	calle = models.CharField(max_length=100)
	no_interior = models.CharField(max_length=50, blank=True)
	no_exterior = models.CharField(max_length=50)
	colonia = models.CharField(max_length=100)
	municipio = models.ForeignKey(Municipio)
	cp = models.CharField(max_length=20)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return u'%s, %s, %s' % (self.calle, self.no_exterior, self.colonia)

class UnidadMedida(models.Model):
	codigo = models.CharField(max_length=10)
	nombre = models.CharField(max_length=100)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return self.nombre

class Perfil(models.Model):
	usuario = models.OneToOneField(User)
	avatar = models.ImageField(upload_to='avatar', blank=True, null=True)
	sitio_web = models.URLField(blank=True)
	telefono = models.CharField(max_length=50, blank=True)
	movil = models.CharField(max_length=50, blank=True)
	outlook_user = models.EmailField(blank=True)
	outlook_pass = models.CharField(max_length=50, blank=True)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return self.usuario.username

class Contacto(models.Model):
	nombre = models.CharField(max_length=200)
	telefono = models.CharField(max_length=50, blank=True)
	movil = models.CharField(max_length=50, blank=True)
	email = models.EmailField(blank=True)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return self.nombre

class Sucursal(models.Model):
	nombre = models.CharField(max_length=500)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return self.nombre

class Cliente(models.Model):
	razon_social = models.CharField(max_length=200)
	nombre = models.CharField(max_length=100)
	domicilio = models.ForeignKey(Domicilio, blank=True, null=True)
	contactos = models.ManyToManyField(Contacto, blank=True)
	sucursales = models.ManyToManyField(Sucursal, blank=True)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def total_contactos(self):
		return self.contactos.count()	

	def __unicode__(self):
		return self.razon_social

class Proveedor(models.Model):
	razon_social = models.CharField(max_length=200)
	nombre = models.CharField(max_length=100)
	domicilio = models.ForeignKey(Domicilio, blank=True, null=True)
	contactos = models.ManyToManyField(Contacto, blank=True)
	sucursales = models.ManyToManyField(Sucursal, blank=True)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def total_contactos(self):
		return self.contactos.count()

	def __unicode__(self):
		return self.razon_social

class Insumo(models.Model):
	nombre = models.CharField(max_length=200)
	unidad_medida = models.ForeignKey(UnidadMedida, null=True, blank=True)
	precio = models.DecimalField(max_digits=8, decimal_places=2)
	costo = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	proveedor = models.ForeignKey(Proveedor, blank=True, null=True)
	herreria = models.BooleanField(default=False)

	actualizado = models.DateTimeField(auto_now=True)
	creado = models.DateTimeField(auto_now_add=True)

	is_active = models.DateTimeField(blank=True, null=True)
	origin = models.IntegerField(blank=True, null=True)
	objects = ConstructoraManager()
	def delete(self):
		BorradoLogico(self)

	def __unicode__(self):
		return u'%s - %s - %s' % (self.nombre, self.unidad_medida, self.precio)


@receiver(post_save, sender=User)
def crearPerfil(sender, instance, created, raw, using, update_fields, **kwargs):
	Perfil.objects.get_or_create(usuario=instance)