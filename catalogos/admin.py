from django.contrib import admin

from django.db.models import Model as Modelo
import inspect
import catalogos.models

for x in dir(catalogos.models):
	item = getattr(catalogos.models, x)
	if inspect.isclass(item) and issubclass(item, Modelo) and x != 'User' and not admin.site.is_registered(item):
		admin.site.register(item)